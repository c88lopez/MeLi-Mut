package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

func startServer() error {
	handleMutant()
	handleStats()

	return http.ListenAndServe(fmt.Sprintf(":%d", config.Server.Port), nil)
}

type mutantPostRequestBody struct {
	Dna []string `json:"dna"`
}

func handleMutant() {
	http.HandleFunc("/mutant", mutantHandler)
}

var readAll = ioutil.ReadAll

func mutantHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	if r.Method != "POST" {
		http.NotFound(w, r)
	}

	body, err := readAll(r.Body)
	if err != nil {
		log.Printf("Error reading request body: %s", err)

		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("unexpected_error"))

		return
	}

	parsedBody := &mutantPostRequestBody{}
	json.Unmarshal(body, &parsedBody)

	dna := dna{}
	dna.setSequence(parsedBody.Dna)

	err = dna.validate()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	} else {
		joinedDnaSequence := strings.Join(parsedBody.Dna, "")

		res, err := db.Query(
			"SELECT is_mutant AS isMutant FROM dnas WHERE dna=?",
			joinedDnaSequence,
		)
		if err != nil {
			log.Printf("Error preparing dna search query: %s", err)

			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("unexpected_error"))

			return
		}
		defer res.Close()

		var isMutant bool
		next := res.Next()
		if next {
			err = res.Scan(&isMutant)
			if err != nil {
				log.Printf("Error scanning dna search query result: %s", err)

				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("unexpected_error"))

				return
			}
		} else {
			err = res.Err()
			if err != nil {
				log.Printf("Error getting result: %s", err)

				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("unexpected_error"))

				return
			}

			isMutant = dna.isMutant(parsedBody.Dna)
			stmt, err := db.Prepare("INSERT dnas(dna, is_mutant) VALUE(?, ?)")

			if err != nil {
				log.Printf("Error preparing insert: %s", err)

				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("unexpected_error"))

				return
			}
			defer stmt.Close()

			_, err = stmt.Exec(joinedDnaSequence, isMutant)
			if err != nil {
				log.Printf("Error executing insert: %s", err)

				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("unexpected_error"))

				return
			}

			err = updateStats(isMutant)
			if err != nil {
				log.Printf("Error updating stats: %s", err)

				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("unexpected_error"))

				return
			}
		}

		if isMutant {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusForbidden)
		}
	}
}

func handleStats() {
	http.HandleFunc("/stats", statsHandler)
}

func statsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.NotFound(w, r)
	}

	jsonResponse, _ := json.Marshal(stats)
	w.Write(jsonResponse)
}
