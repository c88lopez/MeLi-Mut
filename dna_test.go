package main

import (
	"errors"
	"fmt"
	"testing"
)

var validateDataset = []struct {
	in  []string
	out error
}{
	{[]string{
		"ATGCGA",
		"CAGTGC",
		"TTATGT",
		"AGAAGG",
		"CCCCTA",
		"TCACTG",
	}, nil},
	{[]string{
		"ATGCGA",
		"CAGTGC",
		"TTATGT",
		"AGAAGG",
		"CCCCTA",
		"TCACTZ",
	}, errors.New("invalid_char")},
	{[]string{
		"ATG",
		"CAG",
		"TCA",
	}, errors.New("bad_matrix")},
	{[]string{
		"ATGC",
		"CAGCC",
		"TCAC",
	}, errors.New("bad_matrix")},
}

func TestValidate(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	for _, ds := range validateDataset {
		dna := &dna{}
		dna.setSequence(ds.in)

		exp := fmt.Sprintf("%s", ds.out)
		out := fmt.Sprintf("%s", dna.validate())

		if out != exp {
			t.Errorf("Fail validating.\nMatrix: %s\nExp: %s\nOut: %s", ds.in, exp, out)
		}
	}
}

var isMutantDataset = []struct {
	in  []string
	out bool
}{
	{[]string{
		"ATGCGA",
		"CAGTGC",
		"TTATGT",
		"AGAAGG",
		"CCCCTA",
		"TCACTG",
	}, true},
	{[]string{
		"AAAATA",
		"CCGGCC",
		"AATTAA",
		"CCGGCC",
		"AATTAA",
		"CGCCCC",
	}, true},
	{[]string{
		"AATTAA",
		"CCGGCC",
		"ACATAA",
		"CCCACC",
		"AATCAA",
		"CCGGCA",
	}, true},
	{[]string{
		"AATTAA",
		"CCGGAC",
		"AATAAA",
		"CCAGAC",
		"AATAAA",
		"CCAGCC",
	}, true},
	{[]string{
		"AATTGA",
		"CCGGCC",
		"AAGAAA",
		"CGAGCC",
		"AATTAA",
		"ACGGCC",
	}, true},
	{[]string{
		"AATTAA",
		"CCGGCC",
		"AATTAA",
		"CCGGCC",
		"AATTAA",
		"CCGGCC",
	}, false},
	{[]string{
		"TTAATT",
		"GGCCGG",
		"TTAATT",
		"GGCCGG",
		"TTAATT",
		"GGCCGG",
	}, false},
}

func TestIsMutant(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	for _, ds := range isMutantDataset {
		dna := &dna{}
		dna.setSequence(ds.in)

		if dna.isMutant(ds.in) != ds.out {
			yesOrNot := ""
			if !ds.out {
				yesOrNot = " not"
			}

			t.Errorf("dna %s should %sbe mutant", ds.in, yesOrNot)
		}
	}
}

var sameCharsDataset = []struct {
	in  []byte
	out bool
}{
	{[]byte("ATGC"), false},
	{[]byte("AAAT"), false},
	{[]byte("ATTT"), false},
	{[]byte("AAAA"), true},
	{[]byte("TTTT"), true},
	{[]byte("TTTTTTTTTTTTTTTTTTTT"), true},
}

func TestSameChars(t *testing.T) {
	for _, ds := range sameCharsDataset {
		if sameChars(ds.in) != ds.out {
			t.Errorf("Failed sameChars on '%s'.\n", ds.in)
		}
	}
}
