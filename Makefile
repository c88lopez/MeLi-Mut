install: goinstall
	@echo "### Done! ###"
.PHONY: install

goinstall:
	@echo "### Installing ###"
	@go install -ldflags "-w"

run:
	@echo "### Running ###"
	@go run main.go dna.go config.go server.go matrices.go dna_stats.go