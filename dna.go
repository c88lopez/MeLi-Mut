package main

import (
	"errors"
)

type dna struct {
	sequence       [][]byte
	elementsPerRow int
}

func (d *dna) setSequence(dna []string) {
	d.elementsPerRow = len(dna)

	for _, row := range dna {
		d.sequence = append(
			d.sequence,
			[]byte(row),
		)
	}
}

// Se mantiene la firma según enunciado, aunque dna guarda la secuencia.
func (d *dna) isMutant(rawDnaSequence []string) bool {
	var matches = 0

	dna := &dna{}
	dna.setSequence(rawDnaSequence)

	rotatedSequence := rotateMatrix(dna.sequence)

	subMatrices := getSubMatrices(dna.sequence, config.ConsecutiveChars)

	for _, rotatedSubMatrix := range getSubMatrices(rotatedSequence, config.ConsecutiveChars) {
		subMatrices = append(subMatrices, rotatedSubMatrix)
	}

	for _, subMatrix := range subMatrices {
		for _, subMatrixRow := range subMatrix {
			if sameChars(subMatrixRow) {
				matches++
				if matches > 1 {
					return true
				}
			}
		}

		if sameChars(getDiagonal(subMatrix)) {
			matches++
			if matches > 1 {
				return true
			}
		}
	}

	return false
}

func (d *dna) validate() error {
	var err error
	validCharsMap := make(map[byte]bool, 0)

	for _, char := range config.ValidChars {
		validCharsMap[char] = true
	}

	rowLength := len(d.sequence[0])

	if rowLength < config.ConsecutiveChars {
		return errors.New("bad_matrix")
	}

	for _, row := range d.sequence {
		for _, char := range row {
			if !validCharsMap[char] {
				return errors.New("invalid_char")
			}

			if rowLength != len(row) {
				return errors.New("bad_matrix")
			}
		}
	}

	return err
}

func sameChars(s []byte) bool {
	c := s[0]

	len := len(s)
	for i := 1; i < len; i++ {
		if c != s[i] {
			return false
		}
	}

	return true
}
