package main

import (
	"io"
	"testing"

	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestInitStats(t *testing.T) {
	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery(`SELECT (.+) FROM dna_stats`).
		WillReturnRows(
			sqlmock.
				NewRows([]string{"MutantDnaCount", "HumanDnaCount", "Ratio"}).
				AddRow(1, 4, 0.25))

	initStats()

	if stats.MutantDnaCount != 1 ||
		stats.HumanDnaCount != 4 ||
		stats.Ratio != 0.25 {
		t.Errorf("Error init stats")
	}
}

func TestInitStatsNoRows(t *testing.T) {
	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery(`SELECT (.+) FROM dna_stats`).
		WillReturnRows(
			sqlmock.
				NewRows([]string{"MutantDnaCount", "HumanDnaCount", "Ratio"}).
				RowError(0, io.EOF))

	err = initStats()
	if err.Error() != "stats_table_empty" {
		t.Errorf("Error init stats with empty table")
	}
}

func TestUpdateStats(t *testing.T) {
	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectPrepare("UPDATE dna_stats").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	stats = &dnaStats{HumanDnaCount: 0, MutantDnaCount: 0, Ratio: 0.0}
	err = updateStats(true)
	if err != nil {
		t.Errorf("Error Updating stats: %s", err)
	}

	if stats.MutantDnaCount != 1 {
		t.Errorf("Error updating stats by incrementing mutant count")
	}
}

func TestUpdateStatsNotMutant(t *testing.T) {
	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectPrepare("UPDATE dna_stats").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	stats = &dnaStats{HumanDnaCount: 0, MutantDnaCount: 0, Ratio: 0.0}
	err = updateStats(false)
	if err != nil {
		t.Errorf("Error Updating stats: %s", err)
	}

	if stats.HumanDnaCount != 1 {
		t.Errorf("Error updating stats by incrementing human count")
	}
}
