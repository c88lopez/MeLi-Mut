CREATE DATABASE meliMut DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

CREATE TABLE `dnas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dna` varchar(255) NOT NULL,
  `is_mutant` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dna_UNIQUE` (`dna`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `meliMut`.`dna_stats` (
  `dna_count` INT UNSIGNED NOT NULL,
  `dna_mutant_count` INT UNSIGNED NOT NULL);

INSERT INTO `meliMut`.`dna_stats` VALUE(0, 0)
