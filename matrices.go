package main

import (
	"fmt"
)

func getSubMatrices(matrix [][]byte, newMatricesElementCount int) [][][]byte {
	elementsPerRow := len(matrix[0])
	subMatricesList := make(map[string][][]byte)

	for rowIndex, row := range matrix {
		startElementIndex := 0
		for {
			if startElementIndex >= newMatricesElementCount-1 {
				break
			}

			subMatrixIndices := getSubMatrixIndexes(
				rowIndex, elementsPerRow, newMatricesElementCount)

			for _, index := range subMatrixIndices {
				mapIndex := fmt.Sprintf("%d%d", index, startElementIndex)
				subMatricesList[mapIndex] = append(
					subMatricesList[mapIndex],
					row[startElementIndex:newMatricesElementCount+startElementIndex])
			}

			startElementIndex++
		}
	}

	var finalSubMatrices [][][]byte
	for key := range subMatricesList {
		finalSubMatrices = append(finalSubMatrices, subMatricesList[key])
	}

	return finalSubMatrices
}

func rotateMatrix(matrix [][]byte) [][]byte {
	return reverseMatrixRows(transposeMatrix(matrix))
}

func transposeMatrix(matrix [][]byte) [][]byte {
	elementsPerRow := len(matrix[0])
	transposed := make([][]byte, elementsPerRow)

	for rowIndex, row := range matrix {
		for colIndex := range row {
			transposed[colIndex] = append(
				transposed[colIndex],
				matrix[rowIndex][colIndex],
			)
		}
	}

	return transposed
}

func reverseMatrixRows(matrix [][]byte) [][]byte {
	elementsPerRow := len(matrix[0])
	reversed := make([][]byte, 0)

	for i := 0; i < elementsPerRow; i++ {
		reversed = append(reversed, reverseSlice(matrix[i]))
	}

	return reversed
}

func reverseSlice(s []byte) []byte {
	reversedSlice := make([]byte, len(s))

	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		reversedSlice[i], reversedSlice[j] = s[j], s[i]
	}

	return reversedSlice
}

func getSubMatrixIndexes(matrixRowIndex, mainMatrixElementsCount, subMatrixElementsCount int) []int {
	var indexes []int

	maxIndex := mainMatrixElementsCount - subMatrixElementsCount

	for index := maxIndex; 0 <= index; index-- {
		if matrixRowIndex-maxIndex-1 > index {
			break
		}

		if matrixRowIndex < index {
			continue
		}

		indexes = append(indexes, index)
	}

	return indexes
}

func getDiagonal(matrix [][]byte) []byte {
	diagonal := make([]byte, 0)
	for index, row := range matrix {
		diagonal = append(diagonal, row[index])
	}

	return diagonal
}
