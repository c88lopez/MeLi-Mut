package main

import (
	"encoding/json"
	"io/ioutil"
)

const file = "config.json"

// ConfigJSON struct
type ConfigJSON struct {
	ValidChars           []string `json:"validChars"`
	ConsecutiveChars     int      `json:"consecutiveChars"`
	ConsecutiveSequences int      `json:"consecutiveSequences"`
	Db                   struct {
		Host     string `json:"host"`
		User     string `json:"user"`
		Password string `json:"password"`
		Database string `json:"database"`
	} `json:"db"`
	Server struct {
		Port int `json:"port"`
	} `json:"server"`
}

// Config struct
type Config struct {
	ValidChars           []byte
	ConsecutiveChars     int
	ConsecutiveSequences int
	Db                   struct {
		Host     string
		User     string
		Password string
		Database string
	}
	Server struct {
		Port int
	}
}

var getConfigContent = ioutil.ReadFile

func loadConfig() (Config, error) {
	var err error
	configJSON := &ConfigJSON{}
	config := Config{}

	configFileContent, err := getConfigContent(file)
	if err != nil {
		return config, err
	}

	json.Unmarshal(configFileContent, &configJSON)

	for _, char := range configJSON.ValidChars {
		config.ValidChars = append(
			config.ValidChars,
			byte(char[0]),
		)
	}

	config.ConsecutiveChars = configJSON.ConsecutiveChars
	config.ConsecutiveSequences = configJSON.ConsecutiveSequences

	config.Db.Host = configJSON.Db.Host
	config.Db.User = configJSON.Db.User
	config.Db.Password = configJSON.Db.Password
	config.Db.Database = configJSON.Db.Database

	config.Server.Port = configJSON.Server.Port

	return config, nil
}
