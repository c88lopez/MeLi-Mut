package main

import (
	"bytes"
	"testing"
)

var transposeDataset = []struct {
	in  [][]byte
	out [][]byte
}{
	{
		[][]byte{
			[]byte("ATGCGA"),
			[]byte("CAGTGC"),
			[]byte("TTATGT"),
			[]byte("AGAAGG"),
			[]byte("CCCCTA"),
			[]byte("TCACTG"),
		},
		[][]byte{
			[]byte("ACTACT"),
			[]byte("TATGCC"),
			[]byte("GGAACA"),
			[]byte("CTTACC"),
			[]byte("GGGGTT"),
			[]byte("ACTGAG"),
		},
	},
}

func TestTransposeMatrix(t *testing.T) {
	for _, ds := range transposeDataset {
		transposed := transposeMatrix(ds.in)

		for i, row := range transposed {
			if !bytes.Equal(row, ds.out[i]) {
				t.Error("Fail transposing matrix")
			}
		}

	}
}

func TestReverseMatrixRows(t *testing.T) {
	in := [][]byte{
		[]byte("TCATCA"),
		[]byte("CCGTAT"),
		[]byte("ACAAGG"),
		[]byte("CCATTC"),
		[]byte("TTGGGG"),
		[]byte("GAGTCA"),
	}

	out := [][]byte{
		[]byte("ACTACT"),
		[]byte("TATGCC"),
		[]byte("GGAACA"),
		[]byte("CTTACC"),
		[]byte("GGGGTT"),
		[]byte("ACTGAG"),
	}

	got := reverseMatrixRows(in)

	for i, row := range got {
		if !bytes.Equal(row, out[i]) {
			t.Errorf("Fail reversing matrix rows.\nExp: %v\nGot: %v", out, got)
			t.FailNow()
		}
	}
}

var rotateDataset = []struct {
	in  [][]byte
	out [][]byte
}{
	{
		[][]byte{
			[]byte("ATGCGA"),
			[]byte("CAGTGC"),
			[]byte("TTATGT"),
			[]byte("AGAAGG"),
			[]byte("CCCCTA"),
			[]byte("TCACTG"),
		},
		[][]byte{
			[]byte("TCATCA"),
			[]byte("CCGTAT"),
			[]byte("ACAAGG"),
			[]byte("CCATTC"),
			[]byte("TTGGGG"),
			[]byte("GAGTCA"),
		},
	},
}

func TestRotateMatrix(t *testing.T) {
	for _, ds := range rotateDataset {
		rotated := rotateMatrix(ds.in)

		for i, row := range rotated {
			if !bytes.Equal(row, ds.out[i]) {
				t.Errorf("Failed Rotating matrix [row %d]. \nExp: %v\nGot: %v",
					i, ds.out, rotated,
				)
				t.FailNow()
			}
		}
	}

}

func TestReverseSlice(t *testing.T) {
	out := []byte("CTGTCA")

	got := reverseSlice([]byte("ACTGTC"))

	if !bytes.Equal(got, out) {
		t.Errorf("Failed reversing slice.\n Exp: %s\nGot: %s", out, got)
	}
}

var subMatricesDataset = []struct {
	in       [][]byte
	elements int
	out      [][][]byte
}{
	{
		[][]byte{
			[]byte("ATGCGA"),
			[]byte("CAGTGC"),
			[]byte("TTATGT"),
			[]byte("AGAAGG"),
			[]byte("CCCCTA"),
			[]byte("TCACTG"),
		},
		4,
		[][][]byte{
			{
				[]byte("ATGC"),
				[]byte("CAGT"),
				[]byte("TTAT"),
				[]byte("AGAA"),
			},
			{
				[]byte("TGCG"),
				[]byte("AGTG"),
				[]byte("TATG"),
				[]byte("GAAG"),
			},
			{
				[]byte("GCGA"),
				[]byte("GTGC"),
				[]byte("ATGT"),
				[]byte("AAGG"),
			},
			{
				[]byte("CAGT"),
				[]byte("TTAT"),
				[]byte("AGAA"),
				[]byte("CCCC"),
			},
			{
				[]byte("AGTG"),
				[]byte("TATG"),
				[]byte("GAAG"),
				[]byte("CCCT"),
			},
			{
				[]byte("GTGC"),
				[]byte("ATGT"),
				[]byte("AAGG"),
				[]byte("CCTA"),
			},
			{
				[]byte("TTAT"),
				[]byte("AGAA"),
				[]byte("CCCC"),
				[]byte("TCAC"),
			},
			{
				[]byte("TATG"),
				[]byte("GAAG"),
				[]byte("CCCT"),
				[]byte("CACT"),
			},
			{
				[]byte("ATGT"),
				[]byte("AAGG"),
				[]byte("CCTA"),
				[]byte("ACTG"),
			},
		},
	},
}

func TestSubMatrices(t *testing.T) {
	for _, ds := range subMatricesDataset {
		outSubMatrixIndexFoundMap := make(map[int]bool)

		for _, subMatrix := range getSubMatrices(ds.in, ds.elements) {

		outSubMatrixFor:
			for outSubMatrixIndex, outSubMatrix := range ds.out {

				if outSubMatrixIndexFoundMap[outSubMatrixIndex] {
					continue
				}

				for subMatrixRowIndex, subMatrixRow := range subMatrix {
					if !bytes.Equal(subMatrixRow, outSubMatrix[subMatrixRowIndex]) {
						continue outSubMatrixFor
					}
				}

				outSubMatrixIndexFoundMap[outSubMatrixIndex] = true
			}
		}

		if len(outSubMatrixIndexFoundMap) != len(ds.out) {
			t.Errorf("Error getting submatrices")
			t.FailNow()
		}
	}
}

var getSubMatrixIndexesDataset = []struct {
	matrixRowIndex          int
	mainMatrixElementsCount int
	subMatrixElementsCount  int
	out                     []int
}{
	{
		0,
		6,
		4,
		[]int{0},
	},
	{
		1,
		6,
		4,
		[]int{0, 1},
	},
	{
		2,
		6,
		4,
		[]int{0, 1, 2},
	},
	{
		3,
		6,
		4,
		[]int{0, 1, 2},
	},
	{
		4,
		6,
		4,
		[]int{1, 2},
	},
	{
		5,
		6,
		4,
		[]int{2},
	},
}

func TestGetSubMatrixIndexes(t *testing.T) {
	for dsi, ds := range getSubMatrixIndexesDataset {
		actualIndexes := getSubMatrixIndexes(
			ds.matrixRowIndex, ds.mainMatrixElementsCount, ds.subMatrixElementsCount)
		actualIndexesLength := len(actualIndexes)

		expectedLength := len(ds.out)

		if actualIndexesLength != expectedLength {
			t.Errorf("Fail getting correct submatrix indexes.\nIndex list length not match. %d - %d",
				actualIndexes, ds.out)
			t.FailNow()
		}

	ActualIndexLoop:
		for i := 0; i < actualIndexesLength; i++ {
			for j := 0; j < expectedLength; j++ {
				if actualIndexes[i] == ds.out[j] {
					break ActualIndexLoop
				}
			}

			t.Errorf(
				"DS[%d] - Fail getting correct submatrix indexes. \nExp %d\nGot %d",
				dsi, ds.out, actualIndexes)
			i = actualIndexesLength + 1
		}
	}
}

var getDiagonalDataset = []struct {
	in  [][]byte
	out []byte
}{
	{
		[][]byte{
			[]byte("ATGC"),
			[]byte("TGCA"),
			[]byte("ATGC"),
			[]byte("ATCG"),
		}, []byte("AGGG"),
	},
	{
		[][]byte{
			[]byte("TGCA"),
			[]byte("GATC"),
			[]byte("GAAC"),
			[]byte("AACC"),
		}, []byte("TAAC"),
	},
	{
		[][]byte{
			[]byte("ACGT"),
			[]byte("ACGT"),
			[]byte("ACGT"),
			[]byte("ACGT"),
		}, []byte("ACGT"),
	},
	{
		[][]byte{
			[]byte("ACTG"),
			[]byte("CATG"),
			[]byte("CTAG"),
			[]byte("CTGA"),
		}, []byte("AAAA"),
	},
}

func TestGetDiagonal(t *testing.T) {
	for _, ds := range getDiagonalDataset {
		got := getDiagonal(ds.in)

		if !bytes.Equal(getDiagonal(ds.in), ds.out) {
			t.Errorf("Failed getting diagonal.\nExp: %s\nGot: %s", ds.out, got)
		}
	}
}
