package main

import (
	"database/sql"
	"fmt"
	"log"
)

var config Config
var db *sql.DB

func main() {
	var err error

	log.Printf("Loading config...")
	config, err = loadConfig()
	if err != nil {
		log.Fatalln("Error loading config")
	}

	db, err = startDbConnectionPool()
	if err != nil {
		log.Fatalln(err)
	}

	initStats()

	log.Printf("Starting server on port %d...", config.Server.Port)
	err = startServer()
	if err != nil {
		log.Fatalln(err)
	}

	defer db.Close()
}

func startDbConnectionPool() (*sql.DB, error) {
	db, err := sql.Open(
		"mysql",
		fmt.Sprintf(
			"%s:%s@tcp(%s:3306)/%s?charset=utf8",
			config.Db.User, config.Db.Password, config.Db.Host, config.Db.Database,
		),
	)

	if err != nil {
		return db, fmt.Errorf("Error connecting to database: %s", err)
	}

	return db, nil
}
