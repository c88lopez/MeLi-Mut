package main

import (
	"errors"
	"fmt"
)

type dnaStats struct {
	MutantDnaCount int     `json:"count_mutant_dna"`
	HumanDnaCount  int     `json:"count_human_dna"`
	Ratio          float32 `json:"ratio"`
}

var stats = &dnaStats{}

func initStats() error {
	query := `SELECT
dna_mutant_count AS MutantDnaCount
, dna_count - dna_mutant_count AS HumanDnaCount
, dna_mutant_count / (dna_count - dna_mutant_count) AS Ratio
FROM dna_stats`

	res, err := db.Query(query)
	if err != nil {
		return fmt.Errorf("Error preparing dna search query: %s", err)
	}

	status := res.Next()
	if !status {
		err = res.Err()
		if err != nil {
			return err
		}

		return errors.New("stats_table_empty")
	}

	return res.Scan(&stats.MutantDnaCount, &stats.HumanDnaCount, &stats.Ratio)
}

func updateStats(isMutant bool) error {
	var err error
	partialQuery := "UPDATE dna_stats SET dna_count=dna_count+1"
	if isMutant {
		partialQuery = fmt.Sprintf(
			"%s, dna_mutant_count=dna_mutant_count+1", partialQuery,
		)
	}

	stmt, err := db.Prepare(partialQuery)
	if err != nil {
		return fmt.Errorf("Error preparing update: %s", err)
	}

	_, err = stmt.Exec()
	if err != nil {
		return fmt.Errorf("Error executing update: %s", err)
	}

	if isMutant {
		stats.MutantDnaCount++
	} else {
		stats.HumanDnaCount++
	}

	stats.Ratio = float32(stats.MutantDnaCount) / float32(stats.HumanDnaCount)

	return nil
}
