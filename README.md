# MeLi-Mut

[![Build Status](https://travis-ci.org/c88lopez/MeLi-Mut.svg?branch=master)](https://travis-ci.org/c88lopez/MeLi-Mut) [![Go Report Card](https://goreportcard.com/badge/github.com/c88lopez/MeLi-Mut)](https://goreportcard.com/report/github.com/c88lopez/MeLi-Mut) [![codecov](https://codecov.io/gh/c88lopez/MeLi-Mut/branch/master/graph/badge.svg)](https://codecov.io/gh/c88lopez/MeLi-Mut)

# Uso

## Config

En el archivo `config.json` Existen varios parametros de configuración

`validChars` Es el listado de caracteres válidos
`consecutiveChars` Cantidad de caracteres consecutivos minimos,
`consecutiveSequences` Cantidad de secuencias minimas,
`db` Configuración de la base de datos
`server` Configuración del servidor (puerto)

## DB

Dentro de la carpeta /db hay un archivo `init.sql` que se deberá ejecutar previamente. Este crea la base de datos, las tablas necesarias e inicializa una de ellas (stats).

## Ejecución

Se puede lograr de dos formar

`make run` ejecuta el proyecto.
`make install` genera un ejecutable listo para utilizar.

# Test

Para correr los tests, dentro del directorio raiz

`go test -v -race -cover`

# URLs

- ec2-18-212-165-173.compute-1.amazonaws.com:4000/mutant
- ec2-18-212-165-173.compute-1.amazonaws.com:4000/stats