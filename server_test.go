package main

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestStartServerPostMutant(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery("SELECT is_mutant AS isMutant FROM dnas").
		WillReturnRows(
			sqlmock.
				NewRows([]string{"isMutant"}).
				RowError(0, io.EOF))

	mock.ExpectPrepare("INSERT dnas").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectPrepare("UPDATE dna_stats").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTGC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Error responding 200 on mutant, got %d", rr.Code)
	}
}

func TestStartServerPostHuman(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery("SELECT is_mutant AS isMutant FROM dnas").
		WillReturnRows(
			sqlmock.
				NewRows([]string{"isMutant"}).
				RowError(0, io.EOF))

	mock.ExpectPrepare("INSERT dnas").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectPrepare("UPDATE dna_stats").
		ExpectExec().
		WillReturnResult(sqlmock.NewResult(1, 1))

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTAC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusForbidden {
		t.Errorf("Error responding 403 on human, got %d", rr.Code)
	}
}

func TestStartServerPostSavedMutant(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery("SELECT is_mutant AS isMutant FROM dnas").
		WillReturnRows(
			sqlmock.
				NewRows([]string{"isMutant"}).
				AddRow(0))

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTAC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusForbidden {
		t.Errorf("Error responding 403 on human, got %d", rr.Code)
	}
}

func TestStartServerUnexpectedErrorBodyRead(t *testing.T) {
	origReadAll := readAll
	readAll = func(r io.Reader) ([]byte, error) {
		return []byte(""), errors.New("Dummy error")
	}

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTAC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusInternalServerError {
		t.Errorf("Error responding 500, got %d", rr.Code)
	}

	readAll = origReadAll
}

func TestStartServerUnexpectedErrorDnaSearch(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery("SELECT is_mutant AS isMutant FROM dnas").
		WillReturnError(errors.New("Dummy"))

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTAC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusInternalServerError {
		t.Errorf("Error responding 500, got %d", rr.Code)
	}
}

func TestStartServerUnexpectedErrorPreparingInsert(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery("SELECT is_mutant AS isMutant FROM dnas").
		WillReturnRows(sqlmock.NewRows([]string{"isMutant"}).RowError(0, io.EOF))

	mock.ExpectPrepare("INSERT").
		WillReturnError(errors.New("Dummy"))

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTAC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusInternalServerError {
		t.Errorf("Error responding 500, got %d", rr.Code)
	}
}

func TestStartServerUnexpectedErrorExecInsert(t *testing.T) {
	config.ValidChars = []byte("ACGT")
	config.ConsecutiveChars = 4
	config.ConsecutiveSequences = 2

	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error opening stub database connection: %s", err)
	}

	db = mockDb
	defer db.Close()

	mock.ExpectQuery("SELECT is_mutant AS isMutant FROM dnas").
		WillReturnRows(sqlmock.NewRows([]string{"isMutant"}).RowError(0, io.EOF))

	mock.ExpectPrepare("INSERT").
		ExpectExec().
		WillReturnError(errors.New("Dummy"))

	reqBody := bytes.NewReader(
		[]byte("{\"dna\":[\"ATGCGA\",\"CAGTAC\",\"TTATGT\",\"AGAAGG\",\"CACCTA\",\"TCACTG\"]}"))
	req, err := http.NewRequest("POST", "/mutant", reqBody)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(mutantHandler)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusInternalServerError {
		t.Errorf("Error responding 500, got %d", rr.Code)
	}
}

func TestStartServerGetStats(t *testing.T) {
	stats = &dnaStats{HumanDnaCount: 1, MutantDnaCount: 1, Ratio: 1.0}

	req, err := http.NewRequest("GET", "/stats", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(statsHandler)

	handler.ServeHTTP(rr, req)

	exp := "{\"count_mutant_dna\":1,\"count_human_dna\":1,\"ratio\":1}"
	if rr.Body.String() != exp {
		t.Errorf("Error getting correct stat. \nExp: %s\nGot: %s", exp, rr.Body.String())
	}
}
