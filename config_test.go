package main

import (
	"bytes"
	"testing"
)

type fakeSetup struct {
}

func (f *fakeSetup) getConfigContent(fileName string) ([]byte, error) {
	return []byte(
		"{\"validChars\":[\"A\", \"B\", \"C\"],\"consecutiveChars\": 1,\"consecutiveSequences\": 1}",
	), nil
}

func TestLoadConfig(t *testing.T) {
	f := &fakeSetup{}

	getConfigContent = f.getConfigContent

	config, _ := loadConfig()
	if !bytes.Equal(config.ValidChars, []byte("ABC")) ||
		config.ConsecutiveChars != 1 ||
		config.ConsecutiveSequences != 1 {
		t.Errorf("Fail loading config")
	}
}
